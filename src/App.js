import React from 'react';
import logo from './logo.png';
import BleatsAPI from './bleatsAPI';

class app extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      bleats: [],
    };

    this.handleButtonClick = this.addBleat.bind(this);
    this.handleTextChange = this.setBleatTextValue.bind(this);
    this.handleNameChange = this.setBleatNameValue.bind(this);

    this.nameRef = React.createRef();
    this.textRef = React.createRef();
  };

  setBleatTextValue(e) {
    this.setState({
      newBleatText: e.target.value,
      name: this.state.name,
    });
  }

  setBleatNameValue(e) {
    this.setState({
      newBleatText: this.state.newBleatText,
      name: e.target.value,
    });
  }

  addBleat() {
    const newBleats = this.state.bleats;

    newBleats.push({
      text: this.state.newBleatText,
      name: this.state.name,
    });

    this.setState({
      bleats: newBleats,
      text: null,
      name: null,
    });
  }

  componentWillMount() {
    setInterval(function() {
      BleatsAPI.getBleats({ bleatsPerRequest: 3, shouldReturnError: false }) //
        .then(function(result) {
          this.setState({
            bleats: this.state.bleats.concat(result)
          });
        }.bind(this));
    }.bind(this), 3000)
  }

  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <header>Bleater <img src={logo} width={50} height={50} /></header>
        <form>
          <div>
            <input placeholder="Anonymous" name="name" ref={this.nameRef} onChange={this.handleNameChange} />
          </div>
          <div>
            <input placeholder="What's happening?" name="text" ref={this.textRef} onChange={this.handleTextChange}/>
          </div>
          <button onClick={this.handleButtonClick} disabled={!this.state.newBleatText}>Bleat</button>
        </form>
        <div style={{ maxHeight: 400, overflow: 'scroll' }}>
          {this.state && this.state.bleats.reverse().map(function(bleat) {
            return <Bleat name={bleat.name} text={bleat.text} />
          })}
        </div>
      </div>
    );
  }
}

class Bleat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name || 'Anonymous',
    }
  }

  render() {
    return (
        <div>
          <div style={{ fontWeight: 'bold' }}>{this.state.name}</div>
          <div>{this.props.text}</div>
        </div>
    );
  }
}

export default app;

