/** Warning - The code in the file should not be altered **/
import { loremIpsum } from "lorem-ipsum";

let idCounter = 0;

function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateBleat() {
  const id = idCounter;

  idCounter++;

  return {
	id,
	text: loremIpsum(),
	name: ['Mary', 'Bill', 'Jane'][randomNumber(0, 3)],
  }
}

const BleatsAPI = {
  getBleats: function({ bleatsPerRequest, shouldReturnError }) {
	if (shouldReturnError) {
	  return Promise.reject(new Error('There was an error whilst attempting to retrieve more bleats'));
	}

	const bleats = [];

	for (let i = 0; i < bleatsPerRequest; i++) {
	  bleats.push(generateBleat());
	}

	return Promise.resolve(bleats);
  }
};

export default BleatsAPI;