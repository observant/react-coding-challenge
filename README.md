# React Coding Challenge

## Welcome to Bleater!

A junior developer has attempted to develop a Twitter 'clone' in React, using examples from the internet. Unfortunately, some of the examples that they used were out of date and they made several mistakes along the way.

The application should retrieve 'bleats' periodically from the BleatsAPI and display them on the screen in reverse order. Additionally, any bleats added by the user should be appended to the list (stored locally in memory) and displayed on the screen. When a bleat is added, the 'text' field should be cleared to allow for the next bleat. As more bleats are retrieved from the API they should be added to the existing list.

#### Challenge:
 - Make the application work as intended
 - Correct any mistakes that you can see in the code
 - Update the code to use current and best practices

#### Optional challenges:
 - Add code to handle the case where the API produces an error - the application content below the header should be
   replaced with the error message returned from the API (shouldReturnError can be set to true to test this)
 - Introduce a 'higher order component'
 - Add simple styles to the application using your preferred styling method
 - Add functionality to allow the user to delete an existing bleat from the list

#### Bonus points:
 - Use React Hooks


You are welcome to use your own code style guide and conventions. We like Airbnb's React/JSX style guide.

https://github.com/airbnb/javascript/tree/master/react
https://www.npmjs.com/package/eslint-config-airbnb


Please feel free to add as many comments to the code as you like.

Finally, there is no one 'correct' way of solving this challenge, it is simply a way for us to start a conversation!


Enjoy!



## Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started

#### Run `npm install`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
